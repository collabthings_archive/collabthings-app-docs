import * as pluralize from 'pluralize';

import CTApi from 'collabthings-api';
import CTDocsApp from './modules/app';

/**
* @Method: Returns the plural form of any noun.
* @Param {string}
* @Return {string}
*/
export function getPlural(str: any): string {
  return pluralize.plural(str)
}

export { CTDocsApp, CTApi };

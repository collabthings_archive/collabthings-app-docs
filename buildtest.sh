#!/bin/bash

echo COLLABTHINGS-APP-DOCS BUILD

if [ ! -d node_modules ]; then
	npm install
fi

if [ ! -L node_modules/collabthings-api ]; then
	npm install ../collabthings-api
fi

if tsc; then 
	#cp *.json dist/
	#cp -r client dist/
	
	export ssb_appname=ssb-test
	export HOME=$(pwd)/../collabthings-api/testenv/001 
	echo HOME ${HOME}
	npm run test
else
	exit 1;
fi

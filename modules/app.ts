import * as CT from 'collabthings-api';
import CTDocsApi from './api';
import express from 'express';
import path from 'path';

export default class CTDocsApp {
    private api: CTDocsApi = new CTDocsApi();
	
    constructor() {
       	console.log("Setting up CTDocs");
    }

    getApi(): CTDocsApi {
        return this.api;
    }

    stop() {
        this.api.stop();
    }

    register(ctapi: CT.CTApi) {
    	console.log("Registering ctdocs");
    	var info: CT.CTAppInfo = new CT.CTAppInfo();
    	info.name = "docs";
    	info.type = "amazin'";
    	info.settings = {
    		that: "is",
    		cnn: "OK?"
    	};
    	
    	info.static = (exp: express.Application) => {
    		var staticdir: string = path.join(__dirname, '../client');
    		console.log("CTDocs init static " + staticdir);
  	        exp.use("/docs", express.static(staticdir, {
	        	setHeaders: function(res, path) {
	        		console.log("ctdocs request " + path);
	        		if (path.indexOf("download") !== -1) {
	        			res.attachment(path)
	        		}
	        	},
			}));    		
    	};
    	
    	info.api = () => {
    	};
    	
    	
    	ctapi.addApp(info);
    	console.log("Registered ctdocs");
    }
}

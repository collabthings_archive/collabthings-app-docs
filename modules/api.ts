import express from 'express';
import * as serveStatic from 'serve-static';
import * as bodyParser from 'body-parser';
import * as CTApp from './app';
import { Server } from 'net';
import { CTMessageContent, CTApi } from 'collabthings-api';

export default class CTDocsApi {
    exp: express.Application;
    expserver: Server;
	api: CTApi;
	
    init() {
        this.exp = express();
        this.exp.use(bodyParser.json());
        this.initExp();
    }

    info():any {
        var info:any = {};
        info.hello = "Hello!";
        return info;
    }

    stop() {
    	if(this.expserver) {
        	this.expserver.close();
        }
    }

    initExp() {
    	var self: CTDocsApi = this;
    	
        var urlencodedParser = bodyParser.urlencoded({
            extended: false
        });

        this.exp.post("/send", urlencodedParser, function(req, res) {
            console.log("\"info\" called " + JSON.stringify(req.body));
            var content: CTMessageContent = req.body;
            self.api.addMessage(content, content.module, (err: string, msg: string, hash: string) => {
                console.log("message added " + JSON.stringify(msg));
                res.send(JSON.stringify(msg));
            });
        });

        this.exp.get("/info", function(req, res) {
            console.log("\"info\" called");
            res.send(JSON.stringify(self.info()));
        });
    }
}
